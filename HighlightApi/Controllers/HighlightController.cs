﻿using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Jering.Web.SyntaxHighlighters.Prism;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using SyntaxBot.Services.Abstract;

namespace SyntaxBot.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HighlightController : ControllerBase
    {
        private const string CODE_VIEW = @"Views\Code.cshtml";

        private const string PRISM_CSS = @"wwwroot\css\prism.min.css";

        private const string TEST_FILE = @"controllers\TestFile.txt";

        /// <summary>
        /// Сервис конвертации изображений.
        /// </summary>
        private readonly IHtmlToImageConverter _htmlToImageConverter;

        /// <summary>
        /// Сервис подсвечивания кода.
        /// </summary>
        private readonly IPrismService _prismService;

        public HighlightController(IHtmlToImageConverter htmlToImageConverter, IPrismService prismService)
        {
            _htmlToImageConverter = htmlToImageConverter;
            _prismService = prismService;
        }

        // GET api/highlight/test
        [HttpGet]
        public async Task<ActionResult> Test()
        {
            var csharpUri = $"{Request.Scheme}://{Request.Host + Url.Action("csharp")}";

            var client = new RestClient(csharpUri);

            string code = await System.IO.File.ReadAllTextAsync(TEST_FILE);

            var request = new RestRequest();
            request.Method = Method.POST;
            request.AddJsonBody(code);

            IRestResponse<MemoryStream> response = 
                await client.ExecutePostTaskAsync<MemoryStream>(request);

            byte[] imageBytes = response.RawBytes;

            if (response.StatusCode == HttpStatusCode.BadRequest
                || imageBytes == null || imageBytes.Length == 0)
            {
                return BadRequest();
            }

            return File(imageBytes, "image/png");
        }

        /// <summary>
        /// Подсвечивает исходный код.
        /// </summary>
        /// <param name="code">Исходный код, который необходимо подсветить.
        /// Количество символов от 0 до 5000.</param>
        /// <returns>Bad request, если код не соответствует требованиям, иначе png.</returns>
        // POST api/highlight/csharp
        [HttpPost]
        public async Task<ActionResult> Csharp([FromBody] string code)
        {
            if (string.IsNullOrWhiteSpace(code) || code.Count() > 5000)
            {
                return BadRequest();
            }

            code = code.Trim();

            // Получаем стиль и html представления.
            string css = await System.IO.File.ReadAllTextAsync(PRISM_CSS);
            string codeView = await System.IO.File.ReadAllTextAsync(CODE_VIEW);

            var width = 1024;

            // Измеряем ширину текста.
            using (var bmp = new Bitmap(1,1))
            using (var grapghics = Graphics.FromImage(bmp))
            {
                
                var Consolas = new Font(
                    FontFamily.Families.SingleOrDefault(f => f.Name == "Consolas")
                    , 8.25f, FontStyle.Regular
                    );
            
                width = (int)grapghics.MeasureString(code, Consolas).Width;
                Consolas.Dispose();
            }

            var language = "csharp";

            // Подсвечиваем код при помощи Prism.
            code = await _prismService.HighlightAsync(code, language);

            // Формируем итоговый html.
            codeView = codeView
                .Replace("css_placeholder", css)
                .Replace("language_placeholder", "language-" + language)
                .Replace("code_placeholder", code);

            // Конвертируем полученный html в изображение с указанной шириной.
            Stream stream = _htmlToImageConverter.Convert(codeView, ImageExtension.Png, width);

            return File(stream, "image/png");
        }
    }
}