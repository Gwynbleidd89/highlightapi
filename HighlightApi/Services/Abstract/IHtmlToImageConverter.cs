﻿using System.Drawing;
using System.IO;

namespace SyntaxBot.Services.Abstract
{
    public enum ImageExtension
    {
        Jpg,
        Png,
        Bmp,
        Gif
    }

    /// <summary>
    /// Определяет метод конвертации html в поток изображения.
    /// </summary>
    public interface IHtmlToImageConverter
    {
        Stream Convert(string html, ImageExtension extension, int width);
    }
}