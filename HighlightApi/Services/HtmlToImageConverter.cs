﻿using CoreHtmlToImage;
using SyntaxBot.Services.Abstract;
using System.IO;

namespace SyntaxBot.Services
{
    public sealed class HtmlToImageConverter : IHtmlToImageConverter
    {
        /// <summary>
        /// Конвертирует html в изображение и помещает его в поток.
        /// </summary>
        /// <param name="html">Html код.</param>
        /// <param name="extension">Только Jpg/Png, при несоответствии вернется Jpg.</param>
        /// <returns>Поток с изображением переданного формата.</returns>
        public Stream Convert(string html, ImageExtension extension, int width)
        {
            ImageFormat format = extension == ImageExtension.Png
                ? ImageFormat.Png
                : ImageFormat.Jpg;

            var converter = new HtmlConverter();
            byte[] imageBytes = converter.FromHtmlString(html, width, format);

            var stream = new MemoryStream(imageBytes);

            return stream;          
        }
    }
}